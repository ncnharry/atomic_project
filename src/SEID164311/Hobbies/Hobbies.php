<?php


namespace App\Hobbies;
use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class Hobbies extends Database {


    public $id;
    public $name;
    public $hobbies;


    public function setData($dataArray){

        if (array_key_exists("id", $dataArray)){

            $this->id = $dataArray['id'];

        }
        if (array_key_exists("name", $dataArray)){

            $this->name = $dataArray['name'];

        }
        if (array_key_exists("hobbies", $dataArray)){

            $this->hobbies = $dataArray['hobbies'];

        }
    } // end of set data


    public function store(){

       $sqlQuery = "INSERT INTO `hobbies` (`name`, `hobbies`) VALUES (?,?); ";

       $dataArray = array ($this->name, $this->hobbies);

       $sth = $this->dbh->prepare($sqlQuery);

       $results = $sth->execute($dataArray);

        if ($results) {
            Message::message("Data inserted successfull");
        } else {
            Message::message("Error: Data has not been inserted");

        }
    }// end of store class



    public function index(){

        $sqlQuery = "select * from hobbies where is_trashed='No'";
        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();


    }// end of index




    public function view(){

        $sqlQuery = "select * from hobbies WHERE id=" .$this->id;


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetch();


    }// end of view method



    public function update(){

    $sqlQuery = "UPDATE hobbies SET name = ?,hobbies = ? WHERE id = $this->id;";

//    Utility::dd($sqlQuery);

    $dataArray = array($this->name,$this->hobbies);

    $sth = $this->dbh->prepare($sqlQuery);

    $result = $sth->execute($dataArray);

    if ($result){
          Message::message("Success! Data has been updated successfully");
    }else{
       Message::message("Failure! Data has not been updated due to error");
    }

 } // end of update method

    public function trash(){
        $sqlQuery = "UPDATE hobbies SET is_trashed = NOW() WHERE id = $this->id;";

         $result = $this->dbh->exec($sqlQuery);
        if ($result){
            Message::message("Success! Data has been trashed successfully");
        }else{
            Message::message("Failure! Data has not been trashed due to error");
        }

    } // end of update method

    public function trashed (){

        $sqlQuery = "Select * from hobbies where is_trashed<>'No'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;
    }

public function recover(){
        $sqlQuery = "UPDATE hobbies SET is_trashed='No' WHERE id = $this->id;";

    $result = $this->dbh->exec($sqlQuery);

    if($result){
        Message::message("Success! Data has been recovered Successfully!");
    }
    else{
        Message::message("Error! Data has not been recovered.");

    }

} // end of recover method


    public function delete()
    {
        $sqlQuery = "DELETE from hobbies WHERE id = $this->id;";
        $result = $this->dbh->exec($sqlQuery);

        if ($result) {
            Message::message("Success! Data has been deleted Successfully!");
        } else {
            Message::message("Error! Data has not been deleted.");

        }

    } // end of delete method

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byHobbies']) )  $sql = "SELECT * FROM `hobbies` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobbies` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byHobbies']) ) $sql = "SELECT * FROM `hobbies` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byHobbies']) )  $sql = "SELECT * FROM `hobbies` WHERE `is_trashed` ='No' AND `hobbies` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobbies);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobbies);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sqlQuery = "SELECT * from hobbies  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $someData  = $sth->fetchAll();

        return $someData;


    } // end of index paginator method
    

}// end of  Hobbies class