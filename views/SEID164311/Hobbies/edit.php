<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Utility\Utility;
use App\Hobbies\Hobbies;



if(!isset($_GET['id'])) {

    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}

$obj = new Hobbies();
$obj->setData($_GET);
$singleData = $obj->view();

$hobbiesArray = explode(", ", $singleData->hobbies);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Hobbies  list</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: #1b6d85">Single Hobbies Information</h2>
            <form class="form-horizontal" action="update.php" method="post">
                <div class="form-group">
                    <label for="name" >Person Name</label>

                    <input type="text" class="form-control" name="name" value="<?php echo $singleData->name ?>" >
                </div>


             <div class="form-group">
             <label for="hobbies" >Hobbies</label>
             <input type="checkbox"  name="Hobbies[]" value="Gardening" <?php if(in_array("Gardening",$hobbiesArray)) echo "checked" ?>> Gardening
             <input type="checkbox"  name="Hobbies[]" value="Collecting Books" <?php if(in_array("Colleting Books", $hobbiesArray)) echo "checked" ?>> Collecting Books
             <input type="checkbox"  name="Hobbies[]" value="Travelling" <?php if (in_array('Travelling', $hobbiesArray)) echo "checked" ?>> Travelling
             <input type="checkbox"  name="Hobbies[]" value="Bycycling" <?php if (in_array('Bycycling', $hobbiesArray)) echo "checked" ?>> Bycycling
             <input type="checkbox"  name="Hobbies[]" value="Gaming" <?php if (in_array('Gaming', $hobbiesArray)) echo "checked" ?>> Gaming
             <br>


     <!-- Send id with hidden mode to update associated id -->
            <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
     <!-- Send id with hidden mode to update associated id -->

            </div>

                <input class="btn btn-success" type="submit" value="submit">
        </div>

    </div>
    <div class="col-md-3"></div>
</div>
</body>
</html>