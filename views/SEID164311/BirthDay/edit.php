<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

//use App\Message\Message;
//$msg = Message::message();
//echo "<div>  <div id='message'>  $msg </div>   </div>";

$obj = new App\BirthDay\BirthDay();
$obj->setData($_GET);
$singleData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>user list</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: #1b6d85">Update Birth Date Information</h2>
            <form class="form-horizontal" action="update.php"; method="post">
                <div class="form-group">
                    <label for="booktitle" >Name</label>

                    <input type="text" class="form-control" name="name" value="<?php echo $singleData->name ?>" >
                </div>


                <div class="form-group">
                    <label for="birthdate" >Date of Birth</label>

                    <input type="date" class="form-control" name="dob" value="<?php echo $singleData->birthdate ?>">

                    <!-- Send id with hidden mode to update associated id -->
                    <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
                    <!-- Send id with hidden mode to update associated id -->

                </div>

                <input class="btn btn-success" type="submit" value="Update">
        </div>

    </div>
    <div class="col-md-3"></div>
</div>
</body>
</html>