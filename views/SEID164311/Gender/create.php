<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;

$msg = Message::message();

echo "<div style='height: 12px' class='text-center'>  <div class='label label-info text-center' id='message'>  $msg </div>   </div>";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Gender</title>

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: deeppink">Gender Information</h2>
            <form class="form-group" action="store.php" method="post">
                <label>Name</label>
                <input class="form-control col-sm-10"  type="text" name="name" placeholder="Enter Your Name" <?php
                if(empty($_POST))
                {
                    echo "You did not fill out the required fields.";
                }

                ?>>
                <br>
                <br>
                <br>
                <br>
                <label>Gender:</label>
                <input type="radio"  name="gender" value="Male"> Male
                <input type="radio"  name="gender" value="Female"> Female
<!--                <input type="checkbox"  name="Hobbies[]" value="Travelling"> Travelling-->
<!--                <input type="checkbox"  name="Hobbies[]" value="Bycycling"> Bycycling-->
<!--                <input type="checkbox"  name="Hobbies[]" value="Gaming"> Gaming-->
                <br>
                <input class="btn btn-success" type="submit" value="Submit">
                <a href='../../../nav.php' class='btn btn-primary' role='button'>  <span class='glyphicon glyphicon-home'> Return Home</a>
            </form>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>








<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>