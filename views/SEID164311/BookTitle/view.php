<?php
require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
$obj = new \App\BookTitle\BookTitle();


$obj->setData($_GET);
$singleData = $obj->view();

//Utility::dd($allData);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>single book info</title>
</head>

<body>

        <h1 style="color: #000;">Single Book Information </h1>

        <!------------  Nav bar started -------------------------->

        <div class="nav navbar">
            <a href="../../../navigation.php" class="btn btn-primary" role="button">Home Page</a>
            <a href="index.php" class="btn btn-primary" role="button"> << </a>

        </div>




        <!--------------- Nav bar started -------------------->

                <table class="table table-bordered table-striped">

                    <?php
                        echo "
                            <tr><td>ID:</td><td>$singleData->id</td></tr>
                            <tr><td>Book Title : </td><td>$singleData->book_title</td></tr>
                            <tr><td>Author Name: </td><td>$singleData->author_name</td></tr>
                            ";
                    ?>


                </table>



</body>
</html>
