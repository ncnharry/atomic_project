<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Utility\Utility;
$msg = Message::message();

echo "<div style='height: 12px' class='text-center'>  <div class='label label-info text-center' id='message'>  $msg </div>   </div>";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Book list</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 style="text-align: center; color: #1b6d85">Book Information</h2>
            <form class="form-horizontal" action="store.php"; method="post">
                <div class="form-group">
                    <label for="booktitle" >Book Title</label>

                    <input type="text" class="form-control" name="bookTitle" placeholder="Enter Book Name" <?php
                    if(empty($_POST))
                    {
                        echo "You did not fill out the required fields.";
                    }

                    ?>>
                </div>


                <div class="form-group">
                    <label for="authorname" >Author Name</label>

                    <input type="text" class="form-control" name="authorName" placeholder="Enter Author Name" <?php
                    if(empty($_POST))
                    {
                        echo "You did not fill out the required fields.";
                    }

                    ?>>
                </div>

                <input class="btn btn-success" type="submit" value="submit">
                <a href='index.php' class='btn btn-primary' role='button'>  <span class='glyphicon glyphicon-home'> Return Home</a>

        </div>

    </div>
    <div class="col-md-3"></div>
</div>



<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>
    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>




</body>
</html>