<?php
include_once ('../../../vendor/autoload.php');
use App\OrganizationSummary\OrganizationSummary;

$obj= new OrganizationSummary();
$allData=$obj->index();

$trs="";
$sl=0;

    foreach($allData as $row) {
        $id =  $row->id;
        $name = $row->user_name;
        $orgname =$row->organization_name;
        $summary =$row->summary;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='250'> $name </td>";
        $trs .= "<td width='250'> $orgname </td>";
        $trs .= "<td width='250'> $summary </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Name</th>
                    <th align='left' >Organization Name</th>
                    <th align='left' >Summary</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('OrganizationSummary.pdf', 'D');