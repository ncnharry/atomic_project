<?php


require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();


//Utility::dd($_FILES);


$fileName =time(). $_FILES['image'] ['name'];

$source = $_FILES['image']['tmp_name'];

$destination = "Images/".$fileName;

move_uploaded_file($source, $destination);


$_POST['profilePicture'] = $fileName;

$objProfilePicture->setData($_POST);

$objProfilePicture->store();

Utility::redirect("create.php");