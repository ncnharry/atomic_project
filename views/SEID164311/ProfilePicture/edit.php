<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\ProfilePicture\ProfilePicture;

$obj = new ProfilePicture();

$obj->setData($_GET);
$singleData = $obj->view();


$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">

    <title>Edit Profile Picture</title>
    <style>
        body{
            background-color: #1b6d85;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <h1 style="color:deeppink; text-align:center" >Profile Picture Entry </h1>
            <form class="form-group" enctype="multipart/form-data" action="update.php" method="post">
                Enter Person's Name
                <br>
                <input type="text" class="form-control" name="name" value="<?php echo $singleData->name?>">
                <br>
                Enter Profile Picture:
                <input type="file" name="image" accept=".jpg, .png, .jpeg">
                <img src="Images/<?php echo $singleData->profile_picture ?>" alt="image" height="300px" width="300px" class="img-responsive">
                <br>
                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
                <input class="btn btn-danger"  type="submit" value="Update">
                <br>


            </form>

        </div>
        <div class="col-md-3"></div>

    </div>



</div>








<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>